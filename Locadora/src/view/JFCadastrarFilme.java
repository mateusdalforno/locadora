package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.bean.Filme;
import model.dao.FilmeDAO;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JSpinner;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFCadastrarFilme extends JFrame {

	private JPanel contentPane;
	private JTextField txtTitulo;
	private JTextField txtCategoria;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFCadastrarFilme frame = new JFCadastrarFilme();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFCadastrarFilme() {
		setTitle("SisLocadora - Cadastrar Filme");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 584, 432);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCadastrarFilme = new JLabel("Cadastrar Filme");
		lblCadastrarFilme.setFont(new Font("Dialog", Font.BOLD, 14));
		lblCadastrarFilme.setBounds(12, 12, 172, 15);
		contentPane.add(lblCadastrarFilme);
		
		JLabel lblTtulo = new JLabel("Título");
		lblTtulo.setBounds(12, 39, 125, 15);
		contentPane.add(lblTtulo);
		
		txtTitulo = new JTextField();
		txtTitulo.setBounds(12, 66, 550, 19);
		contentPane.add(txtTitulo);
		txtTitulo.setColumns(10);
		
		JLabel lblSinopse = new JLabel("Sinopse");
		lblSinopse.setBounds(12, 97, 70, 15);
		contentPane.add(lblSinopse);
		
		JTextArea txtSinopse = new JTextArea();
		txtSinopse.setBounds(22, 124, 540, 100);
		contentPane.add(txtSinopse);
		
		JLabel lblCategoria = new JLabel("Categoria");
		lblCategoria.setBounds(12, 236, 70, 15);
		contentPane.add(lblCategoria);
		
		txtCategoria = new JTextField();
		txtCategoria.setBounds(12, 265, 550, 19);
		contentPane.add(txtCategoria);
		txtCategoria.setColumns(10);
		
		JLabel lblTempo = new JLabel("Tempo");
		lblTempo.setBounds(22, 296, 70, 15);
		contentPane.add(lblTempo);
		
		JSpinner spnTempo = new JSpinner();
		spnTempo.setBounds(22, 323, 60, 20);
		contentPane.add(spnTempo);
		
		JLabel lblImagem = new JLabel("Imagem");
		lblImagem.setBounds(123, 296, 70, 15);
		contentPane.add(lblImagem);
		
		
		
		JRadioButton rdb2d = new JRadioButton("2D");
		rdb2d.setBounds(123, 321, 60, 23);
		contentPane.add(rdb2d);
		
		JRadioButton rdb3d = new JRadioButton("3D");
		rdb3d.setBounds(190, 321, 60, 23);
		contentPane.add(rdb3d);
		
		ButtonGroup imagem = new ButtonGroup();
		imagem.add(rdb2d);
		imagem.add(rdb3d);
		
		JLabel lbludio = new JLabel("Áudio");
		lbludio.setBounds(267, 296, 70, 15);
		contentPane.add(lbludio);
		
		JRadioButton rdbtnDublado = new JRadioButton("Dublado");
		rdbtnDublado.setBounds(267, 321, 94, 23);
		contentPane.add(rdbtnDublado);
		
		JRadioButton rdbtnLegendado = new JRadioButton("Legendado");
		rdbtnLegendado.setBounds(365, 321, 119, 23);
		contentPane.add(rdbtnLegendado);
		
		ButtonGroup audio = new ButtonGroup();
		audio.add(rdbtnDublado);
		audio.add(rdbtnLegendado);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Filme f = new Filme();
				FilmeDAO dao = new FilmeDAO();
				
				f.setTitulo(txtTitulo.getText());
				f.setSinopse(txtSinopse.getText());
				f.setCategoria(txtCategoria.getText());
				f.setTempo(Integer.parseInt(spnTempo.getValue().toString()));
				if(rdb2d.isSelected()) {
					f.setImagem3d(false);
				}else if(rdb3d.isSelected()) {
					f.setImagem3d(true);
				}
				if(rdbtnDublado.isSelected()) {
					f.setDublado(true);
				}else if (rdbtnLegendado.isSelected()) {
					f.setDublado(false);
				}
				
				dao.create(f);
				dispose();
			}
		});
		btnCadastrar.setBounds(20, 359, 136, 25);
		contentPane.add(btnCadastrar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtTitulo.setText(null);
				txtSinopse.setText(null);
				txtCategoria.setText(null);
				spnTempo.setValue(0);
				imagem.clearSelection();
				audio.clearSelection();				
			}
		});
		btnLimpar.setBounds(190, 359, 117, 25);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnCancelar.setBounds(339, 359, 117, 25);
		contentPane.add(btnCancelar);
	}
}
