package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.bean.Filme;
import model.dao.FilmeDAO;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class JFListarFilmes extends JFrame {

	private JPanel contentPane;
	private JTable JTFilmes;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFListarFilmes frame = new JFListarFilmes();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFListarFilmes() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent e) {
				readJTable();
			}
		});
		setTitle("Listar Filmes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 691, 437);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblListarFilmes = new JLabel("Listar Filmes");
		lblListarFilmes.setFont(new Font("Dialog", Font.BOLD, 18));
		lblListarFilmes.setBounds(12, 12, 302, 28);
		contentPane.add(lblListarFilmes);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 52, 639, 238);
		contentPane.add(scrollPane);
		
		JTFilmes = new JTable();
		JTFilmes.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"idFilme", "T\u00EDtulo", "Categoria", "Tempo"
			}
		));
		scrollPane.setViewportView(JTFilmes);
		
		JButton btnCadastrarFilme = new JButton("Cadastrar Filme");
		btnCadastrarFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFCadastrarFilme cf = new JFCadastrarFilme();
				cf.setVisible(true);				
			}
		});
		btnCadastrarFilme.setBounds(22, 317, 155, 25);
		contentPane.add(btnCadastrarFilme);
		
		JButton btnAlterarFilme = new JButton("Alterar Filme");
		btnAlterarFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//verificar a linha selecionada
				if(JTFilmes.getSelectedRow()!= -1) {
					JFAtualizarFilme af = new JFAtualizarFilme((int)JTFilmes.getValueAt(JTFilmes.getSelectedRow(),0));
					af.setVisible(true);	
				} else {
					JOptionPane.showMessageDialog(null, "Selecione um filme!");
				}
				readJTable();				
			}
		});
		btnAlterarFilme.setBounds(206, 317, 140, 25);
		contentPane.add(btnAlterarFilme);
		
		JButton btnExcluirFilme = new JButton("Excluir Filme");
		btnExcluirFilme.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(JTFilmes.getSelectedRow()!= -1) {
					int opcao = JOptionPane.showConfirmDialog(null, "Deseja excluir o filme selecionado?","Exclusão", JOptionPane.YES_NO_OPTION);
					if(opcao == 0) {
						FilmeDAO dao = new FilmeDAO();
						Filme f = new Filme();
						f.setIdFilme((int)JTFilmes.getValueAt(JTFilmes.getSelectedRow(),0));
						dao.delete(f);
					}
				}else {
					JOptionPane.showMessageDialog(null,"Selecione um filme!");
				}
				readJTable();
			}
		});
		btnExcluirFilme.setBounds(380, 317, 140, 25);
		contentPane.add(btnExcluirFilme);
		
		readJTable();
	}
	
	public void readJTable() {
		DefaultTableModel modelo = (DefaultTableModel) JTFilmes.getModel();
		modelo.setNumRows(0);
		FilmeDAO fdao = new FilmeDAO();
		for(Filme f : fdao.read()) {
			modelo.addRow(new Object[] {
					f.getIdFilme(),
					f.getTitulo(),
					f.getCategoria(),
					f.getTempo()
			});
		}
	}
	
}
