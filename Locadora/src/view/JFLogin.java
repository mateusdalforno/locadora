package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class JFLogin extends JFrame {
 
	private JPanel contentPane;
	private JTextField txtUsuario;
	private JPasswordField txtSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFLogin frame = new JFLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFLogin() {
		setTitle("SisLocadora - Tela de Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSislocadoraBem = new JLabel("SISLOCADORA - BEM VINDO!");
		lblSislocadoraBem.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 20));
		lblSislocadoraBem.setBounds(12, 12, 426, 32);
		contentPane.add(lblSislocadoraBem);
		
		JLabel lblInformeSuasCredenciais = new JLabel("Informe suas credenciais de acesso");
		lblInformeSuasCredenciais.setBounds(12, 56, 426, 23);
		contentPane.add(lblInformeSuasCredenciais);
		
		JLabel lblUsurio = new JLabel("Usuário:");
		lblUsurio.setBounds(12, 91, 70, 15);
		contentPane.add(lblUsurio);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(100, 91, 328, 19);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		JLabel lblSenha = new JLabel("Senha:");
		lblSenha.setBounds(12, 134, 70, 15);
		contentPane.add(lblSenha);
		
		txtSenha = new JPasswordField();
		txtSenha.setBounds(100, 132, 328, 19);
		contentPane.add(txtSenha);
		
		JButton btnAcessar = new JButton("Acessar");
		btnAcessar.setBounds(12, 181, 117, 25);
		contentPane.add(btnAcessar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(154, 181, 117, 25);
		contentPane.add(btnCancelar);
		
		JButton btnCadastrarse = new JButton("Cadastrar-se");
		btnCadastrarse.setBounds(12, 233, 154, 25);
		contentPane.add(btnCadastrarse);
		
		JButton btnRecuperarSenha = new JButton("Recuperar senha");
		btnRecuperarSenha.setBounds(191, 233, 189, 25);
		contentPane.add(btnRecuperarSenha);
		
	}
}
